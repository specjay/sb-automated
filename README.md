# Skillbox UE4 Automated homework

Пример проекта, использующего [Automation System](https://docs.unrealengine.com/en-US/Programming/Automation/index.html). 

 1. `/Source/sb_feature_testing/Private/UExampleClassTest.cpp`  - юнит-тест
 2. `/Content/FunctionalTest/FTEST_DestructableBlock.umap` - карта для функционального теста
 3. `/Content/FunctionalTest/BP_DestructableBlockTest` - логика функционального теста

## Утилиты для упрощения запуска тестов в CI, линтеры, юнит-тест фреймворки:

* [ue4 cli](https://github.com/adamrehn/ue4cli)
* [cpplint](https://github.com/cpplint/cpplint)
* [sonar-cxx plugin](https://github.com/SonarOpenCommunity/sonar-cxx)
* [UE4 Linter V2](https://www.unrealengine.com/marketplace/en-US/product/linter-v2)
* [UE4 Style Guide](https://github.com/Allar/ue4-style-guide)
* [Google Test](https://github.com/google/googletest)

## Домашнее задание

 - [ ] Метод  `bool UExampleClassTest::ReturnTrue()`  должен возвращать `true`
 - [ ] Актор `BP_DestructableBlock` должен уничтожаться при столкновении с актором `FirstPersonProjectile`

### Запуск тестов
Window -> Developer Tools -> Session Frontend -> Automation -> Project

 - [ ] Functional Tests
	 - [ ] FTEST_DestructableBlock 
 - [ ] Unit
	 - [ ] ExampleTest
