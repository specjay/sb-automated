// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class sb_feature_testing : ModuleRules
{
	public sb_feature_testing(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
