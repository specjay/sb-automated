// Fill out your copyright notice in the Description page of Project Settings.

#include "CoreMinimal.h"
#include "Misc/AutomationTest.h"
#include "UExampleClass.h"

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FExampleClassTest, "Project.Unit.ExampleTest", EAutomationTestFlags::ApplicationContextMask | EAutomationTestFlags::ProductFilter)
bool FExampleClassTest::RunTest(const FString& Parameters)
{
	// ReturnTrue method must return true
	{
		auto t = NewObject<UExampleClass>();
		TestTrue(TEXT("ReturnTrue method must return true"), t->ReturnTrue());
	}

	return true;
}