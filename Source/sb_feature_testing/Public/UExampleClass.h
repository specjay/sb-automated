// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "UExampleClass.generated.h"

/**
 * 
 */
UCLASS()
class SB_FEATURE_TESTING_API UExampleClass : public UObject
{
	GENERATED_BODY()
	
public:
		bool ReturnTrue();
};
