// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "sb_feature_testingGameMode.generated.h"

UCLASS(minimalapi)
class Asb_feature_testingGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Asb_feature_testingGameMode();
};



