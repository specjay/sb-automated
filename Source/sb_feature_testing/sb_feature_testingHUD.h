// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "sb_feature_testingHUD.generated.h"

UCLASS()
class Asb_feature_testingHUD : public AHUD
{
	GENERATED_BODY()

public:
	Asb_feature_testingHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

