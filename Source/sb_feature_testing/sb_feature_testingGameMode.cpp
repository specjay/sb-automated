// Copyright Epic Games, Inc. All Rights Reserved.

#include "sb_feature_testingGameMode.h"
#include "sb_feature_testingHUD.h"
#include "sb_feature_testingCharacter.h"
#include "UObject/ConstructorHelpers.h"

Asb_feature_testingGameMode::Asb_feature_testingGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = Asb_feature_testingHUD::StaticClass();
}
